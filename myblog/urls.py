from django.conf.urls import url,patterns
from myblog import views

urlpatterns= [
              url("^$", views.index, name="index"),
              url("^about_me/$", views.about_me, name="about_me"),
              url("^photos/$", views.photos, name="photos"),
              url("^videos/$", views.videos, name="videos"),
               url("^apps/$", views.apps, name="apps"),
              url("^blog/$", views.blog, name="blog")
              ]