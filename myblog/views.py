from django.shortcuts import render
from myblog.models import Article


# Create your views here.

def index(request):
  last_five_articles = Article.objects.order_by('-date')[:5]
  return render(request, 'myblog/index.html', {'menu_id':'index', 'articles': last_five_articles},) 

def about_me(request):
    return render(request, 'myblog/about_me.html', {'menu_id':'about_me'})
def photos(request):
    return render(request, 'myblog/photos.html', {'menu_id':'photos'})
def videos(request):
    return render(request, 'myblog/videos.html', {'menu_id':'videos'})
def apps(request):
    return render(request, 'myblog/apps.html', {'menu_id':'apps'})
def blog(request):
    return render(request, 'myblog/blog.html', {'menu_id':'blog'})