(function(){
	window.addEventListener("load", color_title);
})()

function color_title()
{
	var title_chars = $('#blog_header').text().trim().split('')
	var result_html = ''
	for (var ind in title_chars)
	{
		result_html = result_html + '<span style="color:' + get_random_color() + '">'+ title_chars[ind] + '&nbsp;</span>'
	}
	$('#blog_header').html(result_html);
}
function get_random_color()
{
	return '#'+Math.floor(Math.random()*16777215).toString(16);
}