
	var measureMap = {"inch": 1, "cm": 2.54, "ft": 1/12}
	
	function calculate(target)
	{
	    $("input[id!=" + target.id + "]").each(
	          function(index, value){
	             var new_value = target.value  /measureMap[target.id] * measureMap[this.id];
	              $(this).val(new_value);
	          }
	      );
	}
	
	$('body')
	.on("keyup", "input", function(event){
	      
	      if (!$.isNumeric(event.target.value) ) return;
	      calculate(event.target);
	      
	})
	.focus(function(){ $(this).addClass('selected');
	                 if ( $.isNumeric(event.target.value)  ){
	                      calculate(event.target);} 
	                 })
	.blur(function(){ $(this).removeClass('selected');});  
		
