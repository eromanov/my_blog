from django import template

register = template.Library()

@register.inclusion_tag('myblog/menu.html')
def menu(selected_item):
    m = {'menu': [{'id':'index','text':'Home','link':'index'},
                  {'id':'blog','text':'Blog','link':'blog', 'sub':[{'text': 'Programming'}, {'text': 'Design' }]},
                  {'id':'photos','text':'Photos','link':'photos'},
                  {'id':'videos','text':'Videos', 'link':'videos'},
                  {'id':'apps','text':'Apps', 'link':'apps'},
                  {'id':'about_me','text':'About me', 'link':'about_me'}]}
    
    a = [selected for selected in [ mi for mi in m['menu'] if mi['id'] == selected_item]]
    a[0]['selected'] = True
    return m