from django.db import models

class Video(models.Model):
    title = models.CharField(max_length = 200)
    link = models.URLField()
    def __str__(self):
        return self.title
    
    class Meta:
        app_url = "myblog"
        